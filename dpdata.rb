#! /usr/bin/env ruby

require 'json'

ROOT = "#{ENV['HOME']}/mus/"
DEST = 'mus.json'
EXCEPT = ENV['HOME'].include?('/home') ? /\..$|\.$/ : /\.\.$|\.$|iTunes|.DS_Store/

EXT = /\.mp3$|\.ogg$/

# 2 level tree

def load(file, data)
  if File.directory?(file)
    Dir.entries(file).sort.each do |entry|
      f = File.join(file, entry)
      unless f =~ EXCEPT
        res = load(f, {})
        data[entry.gsub(EXT,'')] =  res if res && !res.empty?
      end
    end
    data
  elsif file =~ EXT
    file
  end
end


mus = load(ROOT, {})
flat = {} # needed cause kays can't be added during iteration

mus.each do |art, albs|
  albs.each do |alb, tracks|
    unless tracks.class == Hash
      mus[art][alb] = {alb => tracks}
    else
      tracks.each do |name, file|
        if file.class == Hash
          mus[art][alb].delete(name)
          flat[art] ||= {}
          flat[art][alb] ||= {}
          exp = {}
          file.each {|n, f| exp["#{name}_#{n}"] = f}
          flat[art][alb].merge!(exp)
        end
      end
    end
  end
end

flat.each do |art,albs|
  albs.each do |alb, tracks|
    mus[art][alb] = Hash[mus[art][alb].merge!(tracks).sort]
  end
end


File.open(DEST,'w') {|f| f.puts mus.to_json }
