"use strict"

var dp = {};
var mus = {};


$(function() {
    $('#player').bind('ended', change_track);

    $(document).keypress(keyhandler());

    $.ajax({
	async: false,
	dataType: 'json',
	url: 'mus.json',
	success: function(d) {
	    mus = d;
	}
    });

    var cache = JSON.parse(localStorage.getItem('dplayer'));
    if (_.has(cache,'tracks') && _.has(cache,'pos')) {
	dp = cache;
	show_tracks();
	show_albums(dp.art);
	play_track();
    }

    $("#artsel").autocomplete({
        source: function(request, response) {
            var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex(request.term), "i");
            response($.grep(_.keys(mus), function(art) {
                return matcher.test(art);
            }))
        },

        select: function(e, ui) {
            select_artist(ui.item.label);
        },

        messages: none()
    });

});



function keyhandler() {
    var fmap = {
        w: function() {
            if ($('#arts button').length == 0)
                show_artists();
        },
        z: function() {
            // change color in the player
            $("#player").trigger("focus");
        },
        x: function() {
            search('#artsel');
        },

        y: function() {
            search('#albsel');
        }
    };


    return function(e) {
        if (e.ctrlKey) {
            var key = String.fromCharCode(e.which);
            if (_.has(fmap, key)) {
                fmap[key]();
                e.preventDefault();
            }
        }
    }

}


////  UTILITIES

function none() {
    return {
        noResults: '',
        results: function() {}
    };
};

function button(val, col) {
    return Kiwi.compose(
        '<button id="%" font style="font-size:12px;color:%">%</button>', [val, col, val]
    );
}



function search(el) {
    $("#arts").empty(); // clear space
    $(el).trigger("focus");
    $(el).val(null);
}

function names() {
    return _.keys(dp.tracks)
}


/// SHOW


function show_artists() {
    $.each(mus, function(art) {
	$('#arts').append(button(art, 'blue'));
    });

    $('#arts button').click(function(e) {
        select_artist($(this)[0].id);
        return false;
    });
}


function show_albums(art) {
    $('#albs').append(art + ':');

    $.each(mus[art], function(alb) {
	$('#albs').append(button(alb, 'green'));
    });

    $('#albs button').click({art: art}, function(e) {
	play_album(e.data.art, $(this)[0].id);
	return false;
    });

    $("#albsel").autocomplete({
        source: $("#albs button").map(function() {
            return this.id;
        }).get(),

        select: function(e, ui) {
            play_album(art, ui.item.label);
        },
        messages: none()
    });
}



function show_tracks() {
    $('#art').empty().append(dp.art);
    $('#alb').empty().append(dp.alb);
    $('#tracks').empty();

    if (names().length > 1) {
        $('#tracks').append('Tracks:');
        $.each(dp.tracks, function(name) {
	    $('#tracks').append(button(name, 'red'));
        });

        $('#tracks button').click(function(e) {
	    dp.pos = names().indexOf($(this)[0].id);
	    play_track();
	    return false;
        });
    }
}





function select_artist(art) {
    $('#albs').empty();
    if (_.keys(mus[art]).length > 1) {
        show_albums(art);
        search("#albsel");
    }
    else
	play_album(art, _.keys(mus[art])[0]);
}


//// PLAY


function play_album(art, alb) {
    dp = {art: art, alb: alb, tracks: mus[art][alb], pos: 0};
    show_tracks();
    play_track();
}





function change_track(e) {
    if (++dp.pos < names().length)
	play_track();
    else {
        // play next album
        var albs = _.keys(mus[dp.art]);
        var i = albs.indexOf(dp.alb);
        if (++i < albs.length)
            play_album(dp.art, albs[i])
    }
}


function play_track() {
    // stored when dp.pos is settled
    localStorage.setItem('dplayer', JSON.stringify(dp));
    $('#track').empty().append(names()[dp.pos]);
    $('#player').attr('src', 'file://' + _.values(dp.tracks)[dp.pos]);
    $('#player')[0].play();
    $('#player').trigger('focus');
}
